package sheridan;


import static org.junit.Assert.*;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class FahrenheitTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testconvertfromCelsius() {
		assertTrue("The temperature was not calculated properly" , Fahrenheit.convertfromCelsius(15));
	}

	@Test
	public void ExceptionTestconvertfromCelsius() {
		assertTrue("The temperature was not calculated properly" , Fahrenheit.convertfromCelsius(-13));
	}
	
	@Test
	public void BoundryInTestconvertfromCelsius() {
		assertTrue("The temperature was not calculated properly" , Fahrenheit.convertfromCelsius(1));
	}
	
	@Test
	public void BoundryOutfromCelsius() {
		assertTrue("The temperature was not calculated properly" , Fahrenheit.convertfromCelsius(0));
	}
	
	private void assertTrue(String string, int i) {
		// TODO Auto-generated method stub
		
	}

}
