package sheridan;

public class Fahrenheit {

	public static int convertfromCelsius ( int c) {
		double f = ( 9/5 * c ) - 32 ;
		int fValue = (int) Math.round(f);
		return fValue;
	}
	
	public static void main(String[] args) {
		int num = convertfromCelsius(70);
	     System.out.println("The result is" +num);

	}

}
